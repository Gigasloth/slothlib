package com.gigasloth.slothlib;

import com.gigasloth.slothlib.rx.GenericRxBus;

import org.junit.Test;

import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.*;

public class RxBusTest {

    private class Message {
        private String mText;

        public Message(String mText) {
            this.mText = mText;
        }

        public String getText() {
            return mText;
        }
    }

    @Test
    public void sendMessage_success() throws Exception {
        GenericRxBus<Message> messageBus = new GenericRxBus<>();

         Consumer<Message> messageConsumer = new Consumer<Message>() {
            @Override
            public void accept(Message message) throws Exception {
                System.out.println(message.getText());
                assertEquals("This is a test", message.getText());
            }
        };

        Message testMessage = new Message("This is a test");

        messageBus.send(testMessage);

        messageBus.asObservable().subscribeOn(Schedulers.computation()).subscribe(messageConsumer);
    }
}