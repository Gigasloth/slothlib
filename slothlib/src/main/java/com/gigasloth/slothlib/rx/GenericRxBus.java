package com.gigasloth.slothlib.rx;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class GenericRxBus<T extends Object> {

    Subject<T> mBus = PublishSubject.create();

    public void send(T message) {
        mBus.onNext(message);
    }

    public Observable<T> asObservable() {
        return mBus;
    }
}
