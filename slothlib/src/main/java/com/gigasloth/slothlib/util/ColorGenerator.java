package com.gigasloth.slothlib.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.gigasloth.slothlib.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ColorGenerator {
    private static Map<ColorSet, ColorGenerator> mColorGenerators = new HashMap<>();

    public static ColorGenerator get(ColorSet colorSet, Context context) {
        ColorGenerator colorGenerator = mColorGenerators.get(colorSet);

        if (colorGenerator == null) {
            mColorGenerators.put(colorSet, new ColorGenerator(colorSet, context));
        }

        return mColorGenerators.get(colorSet);
    }

    public enum ColorSet {
        DEFAULT,
        HOLO,
        MATERIAL;

        public List<Integer> getColors(ColorSet colorSet, Context context) {
            switch (colorSet) {
                case DEFAULT:
                    return Arrays.asList(
                            ContextCompat.getColor(context, R.color.color_default_1),
                            ContextCompat.getColor(context, R.color.color_default_2),
                            ContextCompat.getColor(context, R.color.color_default_3),
                            ContextCompat.getColor(context, R.color.color_default_4),
                            ContextCompat.getColor(context, R.color.color_default_5),
                            ContextCompat.getColor(context, R.color.color_default_6),
                            ContextCompat.getColor(context, R.color.color_default_7),
                            ContextCompat.getColor(context, R.color.color_default_8),
                            ContextCompat.getColor(context, R.color.color_default_9));
                case HOLO:
                    return Arrays.asList(
                            ContextCompat.getColor(context, android.R.color.holo_blue_light),
                            ContextCompat.getColor(context, android.R.color.holo_blue_dark),
                            ContextCompat.getColor(context, android.R.color.holo_blue_bright),
                            ContextCompat.getColor(context, android.R.color.holo_green_light),
                            ContextCompat.getColor(context, android.R.color.holo_green_dark),
                            ContextCompat.getColor(context, android.R.color.holo_orange_light),
                            ContextCompat.getColor(context, android.R.color.holo_orange_dark),
                            ContextCompat.getColor(context, android.R.color.holo_purple),
                            ContextCompat.getColor(context, android.R.color.holo_red_light),
                            ContextCompat.getColor(context, android.R.color.holo_red_dark));
                case MATERIAL:
                    return Arrays.asList(
                            ContextCompat.getColor(context, R.color.material_red),
                            ContextCompat.getColor(context, R.color.material_pink),
                            ContextCompat.getColor(context, R.color.material_purple),
                            ContextCompat.getColor(context, R.color.material_deep_purple),
                            ContextCompat.getColor(context, R.color.material_indigo),
                            ContextCompat.getColor(context, R.color.material_blue),
                            ContextCompat.getColor(context, R.color.material_light_blue),
                            ContextCompat.getColor(context, R.color.material_cyan),
                            ContextCompat.getColor(context, R.color.material_teal),
                            ContextCompat.getColor(context, R.color.material_green),
                            ContextCompat.getColor(context, R.color.material_light_green),
                            ContextCompat.getColor(context, R.color.material_lime),
                            ContextCompat.getColor(context, R.color.material_yellow),
                            ContextCompat.getColor(context, R.color.material_amber),
                            ContextCompat.getColor(context, R.color.material_orange),
                            ContextCompat.getColor(context, R.color.material_deep_orange),
                            ContextCompat.getColor(context, R.color.material_brown),
                            ContextCompat.getColor(context, R.color.material_bluegrey)
                    );
                default:
                    return new ArrayList<>();
            }
        }
    }

    private final List<Integer> mColors;
    private final Random mRandom;

    private ColorGenerator(ColorSet colorSet, Context context) {
        mColors = colorSet.getColors(colorSet, context);
        mRandom = new Random(System.currentTimeMillis());
    }

    public int getRandomColor() {
        return mColors.get(mRandom.nextInt(mColors.size()));
    }

    public int genColor(Object key) {
        return mColors.get(Math.abs(key.hashCode()) % mColors.size());
    }
}
